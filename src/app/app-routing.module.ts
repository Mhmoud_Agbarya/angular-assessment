import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'blocks',
    loadChildren: () =>
      import('./style-assessment/style-assessment.module').then(
        (m) => m.StyleAssessmentModule
      ),
  },
  {
    path: 'forum',
    loadChildren: () =>
      import('./users-and-comments/forum.module').then((m) => m.ForumModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
