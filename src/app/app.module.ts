import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StyleAssessmentModule } from './style-assessment/style-assessment.module';
import { ForumModule } from './users-and-comments/forum.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StyleAssessmentModule,
    ForumModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
