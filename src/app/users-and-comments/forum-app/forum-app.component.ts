import { Component, OnInit } from '@angular/core';
import { CommentsService } from '../services/comments.service';
import { UsersService } from '../services/users.service';

export interface User {
  id?: number;
  displayName?: string;
}

@Component({
  selector: 'app-forum-app',
  templateUrl: './forum-app.component.html',
  styleUrls: ['./forum-app.component.scss'],
})
export class ForumAppComponent implements OnInit {
  users: { id: number; displayName: string }[] = [];
  comments: any = [];
  selectedUser: { id?: number; displayName?: string } = {};

  constructor(
    private usersService: UsersService,
    private commentsService: CommentsService
  ) {}

  ngOnInit(): void {
    this.usersService.getUsers().subscribe((users) => {
      for (var i in users) {
        this.users.push({ id: users[i].id, displayName: users[i].displayName });
      }
      console.log(this.users);
    });
    this.commentsService
      .getComments()
      .subscribe((comments) => (this.comments = comments));
  }

  onUserSelected(event: any) {
    let temp = this.users.find((o) => o.id == event);
    console.log(temp);
    if (temp) {
      this.selectedUser = temp;
      this.getUserComments(event);
    }
  }

  getUserComments(userId: string) {
    if(user)
  }
}
