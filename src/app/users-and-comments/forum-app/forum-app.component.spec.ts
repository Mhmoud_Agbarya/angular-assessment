import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumAppComponent } from './forum-app.component';

describe('ForumAppComponent', () => {
  let component: ForumAppComponent;
  let fixture: ComponentFixture<ForumAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForumAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
