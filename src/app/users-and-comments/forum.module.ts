import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForumRoutingModule } from './forum-routing.module';
import { ForumAppComponent } from './forum-app/forum-app.component';
import { HttpClientModule } from '@angular/common/http';
import { SelectUserComponent } from './select-user/select-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ForumAppComponent, SelectUserComponent],
  imports: [
    CommonModule,
    ForumRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class ForumModule {}
