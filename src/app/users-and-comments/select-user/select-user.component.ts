import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-select-user',
  templateUrl: './select-user.component.html',
  styleUrls: ['./select-user.component.scss'],
})
export class SelectUserComponent implements OnInit {
  @Input() users: { id: number; displayName: string }[] = [];
  @Output() event = new EventEmitter<number>();

  form = new FormGroup({
    user: new FormControl(),
  });

  constructor() {}

  ngOnInit(): void {}

  userSelected(event: any) {
    console.log(event.value);
    this.event.emit(event.value);
  }
}
