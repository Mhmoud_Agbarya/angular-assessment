import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import users from '../assets/users.json';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  getUsers() {
    return of(users);
  }
}
