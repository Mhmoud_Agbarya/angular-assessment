import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import Comments from '../assets/comments.json';

@Injectable({
  providedIn: 'root',
})
export class CommentsService {
  constructor() {}

  getComments() {
    return of(Comments);
  }
}
