import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForumAppComponent } from './forum-app/forum-app.component';

const routes: Routes = [{ path: '', component: ForumAppComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForumRoutingModule {}
