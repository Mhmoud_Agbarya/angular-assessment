import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-block-element',
  templateUrl: './block-element.component.html',
  styleUrls: ['./block-element.component.scss'],
})
export class BlockElementComponent implements OnInit {
  @Input() number: number = 0;
  @Input() color: string = '';

  constructor() {}

  ngOnInit(): void {}
}
