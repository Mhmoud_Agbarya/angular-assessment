import { Component, OnInit } from '@angular/core';
import { blocks_db } from '../blocks-db';

@Component({
  selector: 'app-block-page',
  templateUrl: './block-page.component.html',
  styleUrls: ['./block-page.component.scss'],
})
export class BlockPageComponent implements OnInit {
  blocks = blocks_db;

  constructor() {}

  ngOnInit(): void {}
}
