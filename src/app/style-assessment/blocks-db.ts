export const blocks_db = [
  {
    Num: 1,
    Color: '#66cdaa',
  },
  {
    Num: 2,
    Color: 'red',
  },
  {
    Num: 3,
    Color: 'blue',
  },
  {
    Num: 4,
    Color: 'green',
  },
  {
    Num: 5,
    Color: 'rgba(255,165,0,0.47)',
  },
];
