import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StyleAssessmentRoutingModule } from './style-assessment-routing.module';
import { BlockPageComponent } from './block-page/block-page.component';
import { BlockElementComponent } from './block-element/block-element.component';
import { HeaderComponent } from './header/header.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    BlockPageComponent,
    BlockElementComponent,
    HeaderComponent,
    NavBarComponent,
    FooterComponent,
  ],
  imports: [CommonModule, StyleAssessmentRoutingModule],
})
export class StyleAssessmentModule {}
