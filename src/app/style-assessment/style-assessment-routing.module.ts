import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlockPageComponent } from './block-page/block-page.component';

const routes: Routes = [{ path: '', component: BlockPageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StyleAssessmentRoutingModule {}
